# Laravel Application with Docker Compose

## Prerequisites

- Docker
- Docker Compose

## Getting Started

### 1. Clone the repository

```bash
https://gitlab.com/hellocarloscastillo/test-blossom.git
cd test-blossom
```

## Set up environment variables

Create a .env file in the root of your project and add the following environment variables:
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost:8000

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=mariadb
DB_PORT=3306
DB_DATABASE=a6sd5f4as5df
DB_USERNAME=testblossomusr
DB_PASSWORD=

## Start the Docker containers

docker-compose up -d


## Run migrations and seeders

docker-compose exec testblossom bash
php artisan migrate
php artisan db:seed


## Create a password grant client

php artisan passport:client --password


## Access the application

Your application should now be accessible at http://localhost:8000
and documentation http://localhost:8000/api/documentation

## Running Tests

docker-compose exec testblossom php artisan test
