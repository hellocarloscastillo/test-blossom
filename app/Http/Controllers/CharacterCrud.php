<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Http\Request;

/**
 * @OA\Info(title="API de Ejemplo", version="1.0")
 */
class CharacterCrud extends Controller
{
/**
     * @OA\Get(
     *     path="/api/characters",
     *     tags={"Character"},
     *     @OA\Response(response="200", description="List all characters")
     * )
     */
    public function index()
    {
        $characters = Character::all();
        return response()->json($characters);
    }

    /**
     * @OA\Post(
     *     path="/api/characters",
     *     tags={"Character"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="species", type="string"),
     *             @OA\Property(property="type", type="string"),
     *             @OA\Property(property="gender", type="string"),
     *             @OA\Property(property="image", type="string")
     *         )
     *     ),
     *     @OA\Response(response="201", description="Character created")
     * )
     */
    public function store(Request $request)
    {
        $character = Character::create($request->all());
        return response()->json($character, 201);
    }

    /**
     * @OA\Get(
     *     path="/api/characters/{id}",
     *     tags={"Character"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="Get character by ID")
     * )
     */
    public function show($id)
    {
        $character = Character::findOrFail($id);
        return response()->json($character);
    }

    /**
     * @OA\Put(
     *     path="/api/characters/{id}",
     *     tags={"Character"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="status", type="string"),
     *             @OA\Property(property="species", type="string"),
     *             @OA\Property(property="type", type="string"),
     *             @OA\Property(property="gender", type="string"),
     *             @OA\Property(property="image", type="string")
     *         )
     *     ),
     *     @OA\Response(response="200", description="Update character")
     * )
     */
    public function update(Request $request, $id)
    {
        $character = Character::findOrFail($id);
        $character->update($request->all());
        return response()->json($character);
    }

    /**
     * @OA\Delete(
     *     path="/api/characters/{id}",
     *     tags={"Character"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="Delete character")
     * )
     */
    public function destroy($id)
    {
        $character = Character::findOrFail($id);
        $character->delete();
        return response()->json(null, 204);
    }
}
