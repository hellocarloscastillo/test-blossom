<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use Illuminate\Http\Request;

class EpisodeCrud extends Controller
{
   /**
     * @OA\Get(
     *     path="/api/episodes",
     *     tags={"Episode"},
     *     @OA\Response(response="200", description="List all episodes")
     * )
     */
    public function index()
    {
        $episodes = Episode::all();
        return response()->json($episodes);
    }

    /**
     * @OA\Post(
     *     path="/api/episodes",
     *     tags={"Episode"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="air_date", type="string", format="date"),
     *             @OA\Property(property="episode", type="string")
     *         )
     *     ),
     *     @OA\Response(response="201", description="Episode created")
     * )
     */
    public function store(Request $request)
    {
        $episode = Episode::create($request->all());
        return response()->json($episode, 201);
    }

    /**
     * @OA\Get(
     *     path="/api/episodes/{id}",
     *     tags={"Episode"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="Get episode by ID")
     * )
     */
    public function show($id)
    {
        $episode = Episode::findOrFail($id);
        return response()->json($episode);
    }

    /**
     * @OA\Put(
     *     path="/api/episodes/{id}",
     *     tags={"Episode"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="air_date", type="string", format="date"),
     *             @OA\Property(property="episode", type="string")
     *         )
     *     ),
     *     @OA\Response(response="200", description="Update episode")
     * )
     */
    public function update(Request $request, $id)
    {
        $episode = Episode::findOrFail($id);
        $episode->update($request->all());
        return response()->json($episode);
    }

    /**
     * @OA\Delete(
     *     path="/api/episodes/{id}",
     *     tags={"Episode"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="Delete episode")
     * )
     */
    public function destroy($id)
    {
        $episode = Episode::findOrFail($id);
        $episode->delete();
        return response()->json(null, 204);
    }
}
