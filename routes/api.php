<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CharacterCrud;
use App\Http\Controllers\EpisodeCrud;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');



Route::apiResource('characters', CharacterCrud::class);
Route::apiResource('episodes', EpisodeCrud::class);
