<?php

namespace Tests\Feature;

use App\Models\Character;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CharacterControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_all_characters()
    {
        Character::factory()->count(3)->create();

        $response = $this->getJson('/api/characters');

        $response->assertStatus(200)
                 ->assertJsonCount(3);
    }

    /** @test */
    public function it_can_create_a_character()
    {
        $data = [
            'name' => 'Rick Sanchez',
            'status' => 'Alive',
            'species' => 'Human',
            'type' => '',
            'gender' => 'Male',
            'image' => 'http://example.com/image.png',
        ];

        $response = $this->postJson('/api/characters', $data);

        $response->assertStatus(201)
                 ->assertJsonFragment($data);

        $this->assertDatabaseHas('characters', $data);
    }

    /** @test */
    public function it_can_show_a_character()
    {
        $character = Character::factory()->create();

        $response = $this->getJson("/api/characters/{$character->id}");

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'name' => $character->name,
                 ]);
    }

    /** @test */
    public function it_can_update_a_character()
    {
        $character = Character::factory()->create();

        $data = [
            'name' => 'Morty Smith',
            'status' => 'Alive',
            'species' => 'Human',
            'type' => '',
            'gender' => 'Male',
            'image' => 'http://example.com/new_image.png',
        ];

        $response = $this->putJson("/api/characters/{$character->id}", $data);

        $response->assertStatus(200)
                 ->assertJsonFragment($data);

        $this->assertDatabaseHas('characters', $data);
    }

    /** @test */
    public function it_can_delete_a_character()
    {
        $character = Character::factory()->create();

        $response = $this->deleteJson("/api/characters/{$character->id}");

        $response->assertStatus(204);

        $this->assertDatabaseMissing('characters', ['id' => $character->id]);
    }
}
