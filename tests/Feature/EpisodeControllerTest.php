<?php

namespace Tests\Feature;

use App\Models\Episode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EpisodeControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_list_all_episodes()
    {
        Episode::factory()->count(3)->create();

        $response = $this->getJson('/api/episodes');

        $response->assertStatus(200)
                 ->assertJsonCount(3);
    }

    /** @test */
    public function it_can_create_an_episode()
    {
        $data = [
            'name' => 'Pilot',
            'air_date' => '2013-12-02',
            'episode' => 'S01E01',
        ];

        $response = $this->postJson('/api/episodes', $data);

        $response->assertStatus(201)
                 ->assertJsonFragment($data);

        $this->assertDatabaseHas('episodes', $data);
    }

    /** @test */
    public function it_can_show_an_episode()
    {
        $episode = Episode::factory()->create();

        $response = $this->getJson("/api/episodes/{$episode->id}");

        $response->assertStatus(200)
                 ->assertJsonFragment([
                     'name' => $episode->name,
                 ]);
    }

    /** @test */
    public function it_can_update_an_episode()
    {
        $episode = Episode::factory()->create();

        $data = [
            'name' => 'Lawnmower Dog',
            'air_date' => '2013-12-09',
            'episode' => 'S01E02',
        ];

        $response = $this->putJson("/api/episodes/{$episode->id}", $data);

        $response->assertStatus(200)
                 ->assertJsonFragment($data);

        $this->assertDatabaseHas('episodes', $data);
    }

    /** @test */
    public function it_can_delete_an_episode()
    {
        $episode = Episode::factory()->create();

        $response = $this->deleteJson("/api/episodes/{$episode->id}");

        $response->assertStatus(204);

        $this->assertDatabaseMissing('episodes', ['id' => $episode->id]);
    }
}
